package com.tsc.apogasiy.tm.service;

import com.tsc.apogasiy.tm.api.repository.IOwnerRepository;
import com.tsc.apogasiy.tm.api.service.IOwnerService;
import com.tsc.apogasiy.tm.exception.empty.EmptyIdException;
import com.tsc.apogasiy.tm.exception.empty.EmptyIndexException;
import com.tsc.apogasiy.tm.exception.empty.EmptyUserIdException;
import com.tsc.apogasiy.tm.exception.entity.EntityNotFoundException;
import com.tsc.apogasiy.tm.exception.system.IndexIncorrectException;
import com.tsc.apogasiy.tm.model.AbstractOwnerEntity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public abstract class AbstractOwnerService<E extends AbstractOwnerEntity> extends AbstractService<E> implements IOwnerService<E> {

    @Nullable
    private final IOwnerRepository<E> repository;

    public AbstractOwnerService(@Nullable final IOwnerRepository<E> repository) {
        super(repository);
        this.repository = repository;
    }

    @Override
    @NotNull
    public List<E> findAll(@Nullable final String userId) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty()) throw new EmptyUserIdException();
        return repository.findAll(userId);
    }

    @Override
    @NotNull
    public List<E> findAll(@Nullable final String userId, @Nullable final Comparator<E> comparator) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty()) throw new EmptyUserIdException();
        if (comparator == null) return Collections.emptyList();
        return repository.findAll(userId, comparator);
    }

    @Override
    @Nullable
    public E findById(@Nullable final String userId, @Nullable final String id) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty()) throw new EmptyUserIdException();
        if (!Optional.ofNullable(id).isPresent() || id.isEmpty()) throw new EmptyIdException();
        return repository.findById(userId, id);
    }

    @Override
    @Nullable
    public E findByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty()) throw new EmptyUserIdException();
        Optional.ofNullable(index).orElseThrow(EmptyIndexException::new);
        if (index < 0 || index > repository.getSize(userId)) throw new IndexIncorrectException();
        return repository.findByIndex(userId, index);
    }

    public void clear(@Nullable final String userId) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty()) throw new EmptyUserIdException();
        repository.clear(userId);
    }

    public void remove(@NotNull final String userId, @Nullable final E entity) {
        Optional.ofNullable(entity).orElseThrow(EntityNotFoundException::new);
        repository.remove(userId, entity);
    }

    @Override
    @NotNull
    public Integer getSize(@NotNull final String userId) {
        return repository.getSize(userId);
    }

}
