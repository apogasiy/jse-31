package com.tsc.apogasiy.tm.repository;

import com.tsc.apogasiy.tm.api.repository.IProjectRepository;
import com.tsc.apogasiy.tm.enumerated.Status;
import com.tsc.apogasiy.tm.model.Project;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public class ProjectRepository extends AbstractOwnerRepository<Project> implements IProjectRepository {

    @Override
    public boolean existsByName(@NotNull final String userId, @NotNull final String name) {
        return findByName(userId, name) != null;
    }

    @Override
    @Nullable
    public Project findByName(@NotNull final String userId, @NotNull final String name) {
        return findAll(userId).stream()
                .filter(p -> p.getName().equals(name))
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public Project removeByName(@NotNull final String userId, @NotNull final String name) {
        @Nullable final Project project = findByName(userId, name);
        if (!Optional.ofNullable(project).isPresent())
            return null;
        list.remove(project);
        return project;

    }

    @Override
    @Nullable
    public Project startById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final Project project = findById(userId, id);
        if (!Optional.ofNullable(project).isPresent())
            return null;
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    @Nullable
    public Project startByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final Project project = findByIndex(userId, index);
        if (!Optional.ofNullable(project).isPresent())
            return null;
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    @Nullable
    public Project startByName(@NotNull final String userId, @NotNull final String name) {
        @Nullable final Project project = findByName(userId, name);
        if (!Optional.ofNullable(project).isPresent())
            return null;
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    @Nullable
    public Project finishById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final Project project = findById(userId, id);
        if (!Optional.ofNullable(project).isPresent())
            return null;
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Override
    @Nullable
    public Project finishByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final Project project = findByIndex(userId, index);
        if (!Optional.ofNullable(project).isPresent())
            return null;
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Override
    @Nullable
    public Project finishByName(@NotNull final String userId, @NotNull final String name) {
        @Nullable final Project project = findByName(userId, name);
        if (!Optional.ofNullable(project).isPresent())
            return null;
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Override
    @Nullable
    public Project changeStatusById(@NotNull final String userId, @NotNull final String id, @NotNull final Status status) {
        @Nullable final Project project = findById(userId, id);
        if (!Optional.ofNullable(project).isPresent())
            return null;
        project.setStatus(status);
        return project;
    }

    @Override
    @Nullable
    public Project changeStatusByIndex(@NotNull final String userId, @NotNull final Integer index, @NotNull final Status status) {
        @Nullable final Project project = findByIndex(userId, index);
        if (!Optional.ofNullable(project).isPresent())
            return null;
        project.setStatus(status);
        return project;
    }

    @Override
    @Nullable
    public Project changeStatusByName(@NotNull final String userId, @NotNull final String name, @NotNull final Status status) {
        @Nullable final Project project = findByName(userId, name);
        if (!Optional.ofNullable(project).isPresent())
            return null;
        project.setStatus(status);
        return project;
    }

}