package com.tsc.apogasiy.tm.command;

import com.tsc.apogasiy.tm.dto.Domain;
import com.tsc.apogasiy.tm.enumerated.Role;
import com.tsc.apogasiy.tm.exception.entity.EntityNotFoundException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public abstract class AbstractDataCommand extends AbstractCommand {

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

    @NotNull
    public Domain getDomain() {
        @NotNull final Domain domain = new Domain();
        if (!Optional.ofNullable(serviceLocator).isPresent())
            throw new EntityNotFoundException();
        domain.setProjects(serviceLocator.getProjectService().findAll());
        domain.setTasks(serviceLocator.getTaskService().findAll());
        domain.setUsers(serviceLocator.getUserService().findAll());
        return domain;
    }

    public void setDomain(@Nullable final Domain domain) {
        if (!Optional.ofNullable(serviceLocator).isPresent())
            throw new EntityNotFoundException();
        if (Optional.ofNullable(domain).isPresent()) {
            serviceLocator.getUserService().clear();
            serviceLocator.getTaskService().clear();
            serviceLocator.getProjectService().clear();
            serviceLocator.getUserService().addAll(domain.getUsers());
            serviceLocator.getTaskService().addAll(domain.getTasks());
            serviceLocator.getProjectService().addAll(domain.getProjects());
            if (serviceLocator.getAuthService().isAuth())
                serviceLocator.getAuthService().logout();
        }
    }

}