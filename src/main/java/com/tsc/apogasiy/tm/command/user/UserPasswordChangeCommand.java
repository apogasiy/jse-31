package com.tsc.apogasiy.tm.command.user;

import com.tsc.apogasiy.tm.command.AbstractUserCommand;
import com.tsc.apogasiy.tm.exception.entity.UserNotFoundException;
import com.tsc.apogasiy.tm.exception.system.AccessDeniedException;
import com.tsc.apogasiy.tm.model.User;
import com.tsc.apogasiy.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

public class UserPasswordChangeCommand extends AbstractUserCommand {

    @Override
    public @NotNull String getCommand() {
        return "user-change-password";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public @NotNull String getDescription() {
        return "Change user password";
    }

    @Override
    public void execute() {
        final boolean isAuth = serviceLocator.getAuthService().isAuth();
        if (!isAuth)
            throw new AccessDeniedException();
        System.out.println("Enter id: ");
        final String userId = TerminalUtil.nextLine();
        final User user = serviceLocator.getUserService().findById(userId);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        if (currentUserId.equals(user.getId()))
            throw new AccessDeniedException();
        System.out.println("Enter password: ");
        final String password = TerminalUtil.nextLine();
        serviceLocator.getUserService().setPassword(userId, password);
    }

}
