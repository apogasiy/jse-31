package com.tsc.apogasiy.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tsc.apogasiy.tm.command.AbstractDataCommand;
import com.tsc.apogasiy.tm.dto.Domain;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.nio.file.Files;
import java.nio.file.Paths;

public class DataJsonLoadFasterXmlCommand extends AbstractDataCommand {

    @Override
    @NotNull
    public String getCommand() {
        return "data-load-fasterxml-json";
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Load data from json by FasterXML";
    }

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final String json = new String(Files.readAllBytes(Paths.get(serviceLocator.getPropertyService().getDTOJsonFileName())));
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        setDomain(objectMapper.readValue(json, Domain.class));
    }

}
