package com.tsc.apogasiy.tm.command.data;

import org.jetbrains.annotations.NotNull;

public class DataBackupCommand extends DataXmlSaveFasterXmlCommand {

    public static final String NAME = "data-backup";

    @Override
    @NotNull
    public String getCommand() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Save backup to XML";
    }

}
