package com.tsc.apogasiy.tm.command.task;

import com.tsc.apogasiy.tm.command.AbstractTaskCommand;
import com.tsc.apogasiy.tm.enumerated.Sort;
import com.tsc.apogasiy.tm.model.Task;
import com.tsc.apogasiy.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class TaskListShowCommand extends AbstractTaskCommand {

    @Override
    public @NotNull String getCommand() {
        return "task-list";
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @Override
    public @NotNull String getDescription() {
        return "Show all tasks";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getCurrentUserId();
        System.out.println("Enter sort");
        System.out.println(Arrays.toString(Sort.values()));
        final String sort = TerminalUtil.nextLine();
        final List<Task> tasks;
        if (!Optional.ofNullable(sort).isPresent() || sort.isEmpty())
            tasks = serviceLocator.getTaskService().findAll(userId);
        else {
            Sort sortType = Sort.valueOf(sort);
            System.out.println(sortType.getDisplayName());
            tasks = serviceLocator.getTaskService().findAll(userId, sortType.getComparator());
        }
        for (@NotNull final Task task : tasks)
            showTask(task);
    }

}