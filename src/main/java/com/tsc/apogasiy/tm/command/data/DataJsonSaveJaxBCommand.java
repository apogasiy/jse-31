package com.tsc.apogasiy.tm.command.data;

import com.tsc.apogasiy.tm.command.AbstractDataCommand;
import com.tsc.apogasiy.tm.dto.Domain;
import lombok.SneakyThrows;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.FileOutputStream;

public class DataJsonSaveJaxBCommand extends AbstractDataCommand {

    @Override
    @NotNull
    public String getCommand() {
        return "data-save-jaxb-json";
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Save data to json by JAXB";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(serviceLocator.getPropertyService().getDTOJsonFileName());
        marshaller.marshal(getDomain(), fileOutputStream);
        fileOutputStream.close();
    }

}
