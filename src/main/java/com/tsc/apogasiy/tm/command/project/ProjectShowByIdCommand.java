package com.tsc.apogasiy.tm.command.project;

import com.tsc.apogasiy.tm.command.AbstractProjectCommand;
import com.tsc.apogasiy.tm.exception.entity.ProjectNotFoundException;
import com.tsc.apogasiy.tm.model.Project;
import com.tsc.apogasiy.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public class ProjectShowByIdCommand extends AbstractProjectCommand {

    @Override
    public @NotNull String getCommand() {
        return "project-show-by-id";
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @Override
    public @NotNull String getDescription() {
        return "Show project by id";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getCurrentUserId();
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().findById(userId, id);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        showProject(project);
    }

}