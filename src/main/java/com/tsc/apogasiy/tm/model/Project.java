package com.tsc.apogasiy.tm.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.tsc.apogasiy.tm.api.entity.IWBS;
import com.tsc.apogasiy.tm.enumerated.Status;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@NoArgsConstructor
public class Project extends AbstractOwnerEntity implements IWBS {

    @NotNull
    private String name;

    @Nullable
    private String description;

    @Nullable
    private Status status = Status.NOT_STARTED;

    @Nullable
    private Date startDate = null;

    @Nullable
    private Date created = new Date();

    public Project(@NotNull final String userId, @NotNull final String name, @Nullable final String description) {
        this.userId = userId;
        this.name = name;
        this.description = description;
    }

    @Override
    @NotNull
    public String toString() {
        return id + ": " + name;
    }

}
