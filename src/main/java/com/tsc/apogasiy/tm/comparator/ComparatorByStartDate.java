package com.tsc.apogasiy.tm.comparator;

import com.tsc.apogasiy.tm.api.entity.IHasStartDate;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;

public class ComparatorByStartDate implements Comparator<IHasStartDate> {

    private static final ComparatorByStartDate INSTANCE = new ComparatorByStartDate();

    private ComparatorByStartDate() {
    }

    public static ComparatorByStartDate getInstance() {
        return INSTANCE;
    }

    @Override
    public int compare(@Nullable final IHasStartDate o1, @Nullable final IHasStartDate o2) {
        if (o1 == null || o2 == null || o1.getStartDate() == null || o2.getStartDate() == null)
            return 0;
        return o1.getStartDate().compareTo(o2.getStartDate());
    }

}
