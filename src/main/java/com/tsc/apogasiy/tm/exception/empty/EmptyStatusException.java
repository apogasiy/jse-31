package com.tsc.apogasiy.tm.exception.empty;

import com.tsc.apogasiy.tm.exception.AbstractException;

public class EmptyStatusException extends AbstractException {

    public EmptyStatusException() {
        super("Error! Status is empty!");
    }

}
