package com.tsc.apogasiy.tm.exception.empty;

import com.tsc.apogasiy.tm.exception.AbstractException;

public class EmptyIdException extends AbstractException {

    public EmptyIdException() {
        super("Error! Id is empty!");
    }

}
