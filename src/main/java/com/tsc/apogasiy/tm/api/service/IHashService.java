package com.tsc.apogasiy.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IHashService {

    @NotNull String getPasswordSecret();

    @NotNull Integer getPasswordIteration();

}
