package com.tsc.apogasiy.tm.api.repository;

import com.tsc.apogasiy.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    User findByLogin(@NotNull final String login);

    @Nullable
    User findByEmail(@NotNull final String email);

    @Nullable
    User removeById(@NotNull final String id);

    @Nullable
    User removeByLogin(@NotNull final String login);

    boolean userExistsByLogin(@NotNull final String login);

    boolean userExistsByEmail(@NotNull final String email);

}
